
var userName = "test@drugdev.com";
var password = "supers3cret";

function redirectToHomePage() {
  cy.visit('https://sprinkle-burn.glitch.me/');
}

given("the user has the correct credentials", () => {
  redirectToHomePage();
});

given("the user has the incorrect credentials", () => {
  userName = "invalid@gmail.com";
  password="***";
  redirectToHomePage();
});

when("the user enters username", () => {
  cy.get('[name="email"]')
      .type(userName);   
});

then("the user enters password", () => {
  cy.get('[name="password"]')
      .type(password);  
});

then("clicks Login", () => {
  cy.get('[class="f5 dim bn ph3 pv2 mb2 dib br1 white bg-dark-green"]')     
      .click() 
});

then("the user is presented with a welcome message", () => {
  cy.get('article').contains('Welcome Dr I Test')
});

then("the user is presented with a error message", () => {
  cy.get('[id="login-error-box"]').contains('Credentials are incorrect')
});


